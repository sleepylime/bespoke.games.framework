#pragma once

#include "DrawableGameComponent.h"
#include "Transform.h"
#include <map>
#include <vector>

namespace Library
{
	class GameObject : public DrawableGameComponent
	{
		RTTI_DECLARATIONS(GameObject, DrawableGameComponent)

	public:
		GameObject() = default;
		GameObject(Game& game);
		GameObject(Game& game, const std::shared_ptr<Camera>& camera);
		GameObject(const GameObject&) = default;
		GameObject& operator=(const GameObject&) = default;
		GameObject(GameObject&&) = default;
		GameObject& operator=(GameObject&&) = default;
		virtual ~GameObject() = default;

		const Transform& GetTransform() const;
		void SetTransform(const Transform& transform);

		const std::string& Tag() const;
		void SetTag(const std::string& tag);

		const std::map<std::uint64_t, std::vector<std::shared_ptr<GameComponent>>>& Components() const;
		void AddComponent(const std::shared_ptr<GameComponent>& component);
		std::shared_ptr<GameComponent> GetComponent(const std::uint64_t typeID);
		
		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;
		virtual void Draw(const GameTime& gameTime) override;

	private:
		Transform mTransform;
		std::string mTag;
		std::map<std::uint64_t, std::vector<std::shared_ptr<GameComponent>>> mComponents;
	};
}

#include "GameObject.inl"
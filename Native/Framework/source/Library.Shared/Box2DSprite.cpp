#include "pch.h"
#include "Box2DSprite.h"

using namespace std;
using namespace DirectX;
using namespace Microsoft::WRL;

namespace Library
{
#pragma region AbstractBox2DSpriteDef

	const float AbstractBox2DSpriteDef::DefaultFriction = 0.3f;
	const float AbstractBox2DSpriteDef::DefaultRestitution = 0.0f;
	const float AbstractBox2DSpriteDef::DefaultDensity = 1.0f;
	
	AbstractBox2DSpriteDef::AbstractBox2DSpriteDef(const b2BodyDef& bodyDef, const float friction, const float restitution, const float density, const bool isSensor) :
		BodyDef(bodyDef), Friction(friction), Restitution(restitution), Density(density), IsSensor(isSensor)
	{
	}

#pragma endregion

#pragma region Box2DSpriteDef

	template<typename T>
	Box2DSpriteDef<T>::Box2DSpriteDef(const b2BodyDef& bodyDef, const T& shape, const float friction, const float restitution, const float density, const bool isSensor) :
		AbstractBox2DSpriteDef(bodyDef, friction, restitution, density, isSensor),
		mShape(shape)
	{
	}

	template<typename T>
	const b2Shape& Library::Box2DSpriteDef<T>::Shape() const
	{
		return _Shape();
	}

	template<typename T>
	const T& Box2DSpriteDef<T>::_Shape() const
	{
		return mShape;
	}

#pragma endregion

	RTTI_DEFINITIONS(Box2DSprite)

	Box2DSprite::Box2DSprite(Game& game, const shared_ptr<Camera>& camera, const shared_ptr<Texture2D>& texture, const AbstractBox2DSpriteDef& spriteDef, const XMFLOAT2& scale, const ComPtr<ID3D11SamplerState>& samplerState, uint32_t horizontalTileCount, uint32_t verticalTileCount) :
		Sprite(game, camera, texture, samplerState, horizontalTileCount, verticalTileCount)
	{
		CreateBody(spriteDef);
		mBody->SetUserData(this);
		mTransform.SetScale(scale);
	}

	Box2DSprite::Box2DSprite(Game& game, const shared_ptr<Camera>& camera, const shared_ptr<Texture2D>& texture, b2Body* existingBody, const XMFLOAT2& scale, const ComPtr<ID3D11SamplerState>& samplerState, uint32_t horizontalTileCount, uint32_t verticalTileCount) :
		Sprite(game, camera, texture, samplerState, horizontalTileCount, verticalTileCount),
		mBody(existingBody)
	{
		mBody->SetUserData(this);
		mTransform.SetScale(scale);
	}

	Box2DSprite::~Box2DSprite()
	{
		if (mGame->IsShuttingDown() == false && mCreatedBody)
		{
			auto physicsEngine = reinterpret_cast<Box2DComponent*>(mGame->Services().GetService(Box2DComponent::TypeIdClass()));
			assert(physicsEngine != nullptr);
			physicsEngine->BuryBody(*mBody);
		}
	}

	b2Body* Box2DSprite::Body() const
	{
		return mBody;
	}

	void Box2DSprite::Initialize()
	{		
		Sprite::Initialize();
		UpdateTransformFromBody();
	}

	void Box2DSprite::Update(const GameTime& gameTime)
	{
		UNREFERENCED_PARAMETER(gameTime);

		if (mBody->GetType() != b2_staticBody && mBody->IsAwake())
		{
			UpdateTransformFromBody();
		}
	}

	shared_ptr<Box2DSprite> Box2DSprite::CreateBox(Game& game, const shared_ptr<Camera>& camera, const shared_ptr<Texture2D>& texture, FXMVECTOR position, float rotation, const XMFLOAT2& scale)
	{
		return CreateBox(game, camera, texture, XMFLOAT2(XMVectorGetX(position), XMVectorGetY(position)), rotation, scale);
	}

	shared_ptr<Box2DSprite> Box2DSprite::CreateBox(Game& game, const shared_ptr<Camera>& camera, const shared_ptr<Texture2D>& texture, const XMFLOAT2& position, float rotation, const XMFLOAT2& scale)
	{
		b2BodyDef bodyDef;
		bodyDef.type = b2_dynamicBody;
		bodyDef.position.Set(position.x, position.y);
		bodyDef.angle = rotation;

		b2PolygonShape shape;
		shape.SetAsBox(scale.x, scale.y);

		return make_shared<Box2DSprite>(game, camera, texture, Box2DSpriteDef<b2PolygonShape>(bodyDef, shape), scale);
	}

	shared_ptr<Box2DSprite> Box2DSprite::CreateCircle(Game& game, const shared_ptr<Camera>& camera, const shared_ptr<Texture2D>& texture, FXMVECTOR position, float rotation, const float radius)
	{
		return CreateCircle(game, camera, texture, XMFLOAT2(XMVectorGetX(position), XMVectorGetY(position)), rotation, radius);
	}
	
	shared_ptr<Box2DSprite> Box2DSprite::CreateCircle(Game& game, const shared_ptr<Camera>& camera, const shared_ptr<Texture2D>& texture, const XMFLOAT2& position, float rotation, const float radius)
	{
		b2BodyDef bodyDef;
		bodyDef.type = b2_dynamicBody;
		bodyDef.position.Set(position.x, position.y);
		bodyDef.angle = rotation;

		b2CircleShape shape;
		shape.m_radius = radius;

		return make_shared<Box2DSprite>(game, camera, texture, Box2DSpriteDef<b2CircleShape>(bodyDef, shape), XMFLOAT2(radius, radius));
	}

	shared_ptr<Box2DSprite> Box2DSprite::CreateTriangle(Game& game, const shared_ptr<Camera>& camera, const shared_ptr<Texture2D>& texture, FXMVECTOR position, float rotation, const XMFLOAT2& scale)
	{
		return CreateTriangle(game, camera, texture, XMFLOAT2(XMVectorGetX(position), XMVectorGetY(position)), rotation, scale);
	}

	shared_ptr<Box2DSprite> Box2DSprite::CreateTriangle(Game& game, const shared_ptr<Camera>& camera, const shared_ptr<Texture2D>& texture, const XMFLOAT2& position, float rotation, const XMFLOAT2& scale)
	{
		b2BodyDef bodyDef;
		bodyDef.type = b2_dynamicBody;
		bodyDef.position.Set(position.x, position.y);
		bodyDef.angle = rotation;

		const b2Vec2 vertices[] =
		{
			{ scale.x, -scale.y },
			{ 0.0f, scale.y },
			{ -scale.x, -scale.y },
		};

		b2PolygonShape shape;
		shape.Set(vertices, ARRAYSIZE(vertices));		

		return make_shared<Box2DSprite>(game, camera, texture, Box2DSpriteDef<b2PolygonShape>(bodyDef, shape), scale);
	}

	void Box2DSprite::CreateBody(const AbstractBox2DSpriteDef& spriteDef)
	{
		auto physicsEngine = reinterpret_cast<Box2DComponent*>(mGame->Services().GetService(Box2DComponent::TypeIdClass()));
		assert(physicsEngine != nullptr);

		b2World& world = physicsEngine->World();
		mBody = world.CreateBody(&spriteDef.BodyDef);

		b2FixtureDef fixtureDef;
		fixtureDef.shape = &(spriteDef.Shape());
		fixtureDef.friction = spriteDef.Friction;
		fixtureDef.restitution = spriteDef.Restitution;
		fixtureDef.density = spriteDef.Density;
		fixtureDef.filter = spriteDef.Filter;
		fixtureDef.isSensor = spriteDef.IsSensor;
		mBody->CreateFixture(&fixtureDef);
	}

	void Box2DSprite::UpdateTransformFromBody()
	{
		const auto& position = mBody->GetPosition();
		mTransform.SetPosition(position.x, position.y);
		mTransform.SetRotation(mBody->GetAngle());
	}	
}


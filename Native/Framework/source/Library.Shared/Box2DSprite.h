#pragma once

#include "Sprite.h"
#include "VectorHelper.h"
#include <Box2D\Box2D.h>

namespace Library
{	
	class AbstractBox2DSpriteDef abstract
	{
	public:
		static const float DefaultFriction;
		static const float DefaultRestitution;
		static const float DefaultDensity;

		AbstractBox2DSpriteDef(const AbstractBox2DSpriteDef&) = default;
		AbstractBox2DSpriteDef& operator=(const AbstractBox2DSpriteDef&) = default;
		AbstractBox2DSpriteDef(AbstractBox2DSpriteDef&&) = default;
		AbstractBox2DSpriteDef& operator=(AbstractBox2DSpriteDef&&) = default;
		virtual ~AbstractBox2DSpriteDef() = default;

		b2BodyDef BodyDef;
		float Friction;
		float Restitution;
		float Density;
		bool IsSensor;
		b2Filter Filter;

		virtual const b2Shape& Shape() const = 0;

	protected:
		AbstractBox2DSpriteDef(const b2BodyDef& bodyDef, const float friction = DefaultFriction, const float restitution = DefaultRestitution, const float density = DefaultDensity, const bool isSensor = false);
	};

	template <typename T>
	class Box2DSpriteDef : public AbstractBox2DSpriteDef
	{
	public:
		Box2DSpriteDef(const b2BodyDef& bodyDef, const T& shape, const float friction = DefaultFriction, const float restitution = DefaultRestitution, const float density = DefaultDensity, const bool isSensor = false);
		Box2DSpriteDef(const Box2DSpriteDef&) = default;
		Box2DSpriteDef& operator=(const Box2DSpriteDef&) = default;
		Box2DSpriteDef(Box2DSpriteDef&&) = default;
		Box2DSpriteDef& operator=(Box2DSpriteDef&&) = default;
		virtual ~Box2DSpriteDef() = default;

		const b2Shape& Shape() const;

	protected:		
		virtual const T& _Shape() const;

		T mShape;
	};

	class Box2DSprite final : public Sprite
	{
		RTTI_DECLARATIONS(Box2DSprite, Sprite)

	public:
		Box2DSprite(Game& game, const std::shared_ptr<Camera>& camera, const std::shared_ptr<Texture2D>& texture, const AbstractBox2DSpriteDef& spriteDef, const DirectX::XMFLOAT2& scale = Vector2Helper::One, const Microsoft::WRL::ComPtr<ID3D11SamplerState>& samplerState = SamplerStates::TrilinearClamp, std::uint32_t horizontalTileCount = SpriteMaterial::DefaultHorizontalTileCount, std::uint32_t verticalTileCount = SpriteMaterial::DefaultVerticalTileCount);
		Box2DSprite(Game& game, const std::shared_ptr<Camera>& camera, const std::shared_ptr<Texture2D>& texture, b2Body* existingBody, const DirectX::XMFLOAT2& scale = Vector2Helper::One, const Microsoft::WRL::ComPtr<ID3D11SamplerState>& samplerState = SamplerStates::TrilinearClamp, std::uint32_t horizontalTileCount = SpriteMaterial::DefaultHorizontalTileCount, std::uint32_t verticalTileCount = SpriteMaterial::DefaultVerticalTileCount);
		Box2DSprite(const Box2DSprite&) = delete;
		Box2DSprite& operator=(const Box2DSprite&) = delete;
		Box2DSprite(Box2DSprite&&) = default;
		Box2DSprite& operator=(Box2DSprite&&) = default;
		~Box2DSprite();

		b2Body* Body() const;

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;

		static std::shared_ptr<Box2DSprite> CreateBox(Game& game, const std::shared_ptr<Camera>& camera, const std::shared_ptr<Texture2D>& texture, DirectX::FXMVECTOR position, float rotation = 0.0f, const DirectX::XMFLOAT2& scale = Vector2Helper::One);
		static std::shared_ptr<Box2DSprite> CreateBox(Game& game, const std::shared_ptr<Camera>& camera, const std::shared_ptr<Texture2D>& texture, const DirectX::XMFLOAT2& position, float rotation = 0.0f, const DirectX::XMFLOAT2& scale = Vector2Helper::One);
		
		static std::shared_ptr<Box2DSprite> CreateCircle(Game& game, const std::shared_ptr<Camera>& camera, const std::shared_ptr<Texture2D>& texture, DirectX::FXMVECTOR position, float rotation = 0.0f, const float radius = 1.0f);
		static std::shared_ptr<Box2DSprite> CreateCircle(Game& game, const std::shared_ptr<Camera>& camera, const std::shared_ptr<Texture2D>& texture, const DirectX::XMFLOAT2& position, float rotation = 0.0f, const float radius = 1.0f);

		static std::shared_ptr<Box2DSprite> CreateTriangle(Game& game, const std::shared_ptr<Camera>& camera, const std::shared_ptr<Texture2D>& texture, DirectX::FXMVECTOR position, float rotation = 0.0f, const DirectX::XMFLOAT2& scale = Vector2Helper::One);
		static std::shared_ptr<Box2DSprite> CreateTriangle(Game& game, const std::shared_ptr<Camera>& camera, const std::shared_ptr<Texture2D>& texture, const DirectX::XMFLOAT2& position, float rotation = 0.0f, const DirectX::XMFLOAT2& scale = Vector2Helper::One);

	private:
		void CreateBody(const AbstractBox2DSpriteDef& spriteDef);
		void UpdateTransformFromBody();

		b2Body* mBody;
		bool mCreatedBody;
	};
}
#include "pch.h"
#include "MeshRenderingMaterial.h"

using namespace std;
using namespace DirectX;
using namespace Microsoft::WRL;

namespace Library
{
#pragma region MeshRenderingData

	MeshRenderingData::MeshRenderingData(const ComPtr<ID3D11SamplerState>& samplerState) :
		mSamplerState(samplerState)
	{
	}

	ComPtr<ID3D11SamplerState> MeshRenderingData::SamplerState() const
	{
		return mSamplerState;
	}

	void MeshRenderingData::SetSamplerState(const ComPtr<ID3D11SamplerState>& samplerState)
	{
		mSamplerState = samplerState;
	}

	const ID3D11Buffer* MeshRenderingData::VertexBuffer() const
	{
		return mVertexBuffer.Get();
	}

	const ID3D11Buffer* MeshRenderingData::IndexBuffer() const
	{
		return mIndexBuffer.Get();
	}

	uint32_t MeshRenderingData::IndexCount() const
	{
		return mIndexCount;
	}

	shared_ptr<Texture2D> MeshRenderingData::DiffuseMap() const
	{
		return mDiffuseMap;
	}

	void MeshRenderingData::SetDiffuseMap(const shared_ptr<Texture2D>& diffuseMap)
	{
		mDiffuseMap = diffuseMap;
	}	

	void MeshRenderingData::Initialize(ID3D11Device& device, const Mesh& mesh, const shared_ptr<Texture2D>& diffuseMap)
	{
		const vector<XMFLOAT3>& sourceVertices = mesh.Vertices();
		const vector<XMFLOAT3>* const textureCoordinates = mesh.TextureCoordinates().at(0);
		assert(textureCoordinates->size() == sourceVertices.size());

		vector<VertexPositionTexture> vertices;
		vertices.reserve(sourceVertices.size());
		for (size_t i = 0; i < sourceVertices.size(); i++)
		{
			const XMFLOAT3& position = sourceVertices.at(i);
			const XMFLOAT3& uv = textureCoordinates->at(i);
			vertices.emplace_back(XMFLOAT4(position.x, position.y, position.z, 1.0f), XMFLOAT2(uv.x, uv.y));
		}

		D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
		vertexBufferDesc.ByteWidth = sizeof(VertexPositionTexture) * static_cast<uint32_t>(vertices.size());
		vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
		vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

		D3D11_SUBRESOURCE_DATA vertexSubResourceData = { 0 };
		vertexSubResourceData.pSysMem = &vertices[0];
		ThrowIfFailed(device.CreateBuffer(&vertexBufferDesc, &vertexSubResourceData, mVertexBuffer.ReleaseAndGetAddressOf()), "ID3D11Device::CreateBuffer() failed.");

		mesh.CreateIndexBuffer(device, mIndexBuffer.ReleaseAndGetAddressOf());
		mDiffuseMap = diffuseMap;
	}

	void MeshRenderingData::Initialize(ID3D11Device& device, const Mesh& mesh, ContentManager& content, const wstring& diffuseMapFilename)
	{
		auto diffuseMap = content.Load<Texture2D>(diffuseMapFilename);
		Initialize(device, mesh, diffuseMap);
	}

#pragma endregion

	RTTI_DEFINITIONS(MeshRenderingMaterial)

	MeshRenderingMaterial::MeshRenderingMaterial(Game& game) :
		Material(game)
	{
	}

	uint32_t MeshRenderingMaterial::VertexSize() const
	{
		return sizeof(VertexPositionTexture);
	}

	void MeshRenderingMaterial::Initialize()
	{
		Material::Initialize();

		mVertexShader = mGame->Content().Load<VertexShader>(L"Shaders\\MeshRendererVS.cso");
		mVertexShader->CreateInputLayout(mGame->Direct3DDevice(), const_cast<D3D11_INPUT_ELEMENT_DESC*>(VertexPositionTexture::InputElements), VertexPositionTexture::InputElementCount);
		mPixelShader = mGame->Content().Load<PixelShader>(L"Shaders\\MeshRendererPS.cso");

		D3D11_BUFFER_DESC constantBufferDesc = { 0 };
		constantBufferDesc.ByteWidth = sizeof(XMFLOAT4X4);
		constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		ThrowIfFailed(mGame->Direct3DDevice()->CreateBuffer(&constantBufferDesc, nullptr, mConstantBuffer.ReleaseAndGetAddressOf()), "ID3D11Device::CreateBuffer() failed.");
	}

	void MeshRenderingMaterial::BeginDraw()
	{
		Material::BeginDraw();

		auto direct3DDeviceContext = mGame->Direct3DDeviceContext();
		direct3DDeviceContext->VSSetConstantBuffers(0, 1, mConstantBuffer.GetAddressOf());
	}

	void MeshRenderingMaterial::UpdateConstantBuffer(CXMMATRIX worldViewProjectionMatrix)
	{
		mGame->Direct3DDeviceContext()->UpdateSubresource(mConstantBuffer.Get(), 0, nullptr, reinterpret_cast<const float*>(worldViewProjectionMatrix.r), 0, 0);
	}
}
#pragma once

#include <vector>
#include <random>
#include <memory>
#include <cstdint>
#include <wrl.h>
#include <d3d11_2.h>

namespace Library
{
	class PerlinNoise final
	{
	public:
		static void CreateInstance();
		static void Shutdown();

		static std::vector<std::vector<float>> GenerateWhiteNoise(const std::uint32_t width, const std::uint32_t height);
		static std::vector<std::vector<float>> GenerateSmoothNoise(const std::vector<std::vector<float>>& baseNoise, const std::uint32_t octave);
		static std::vector<std::vector<float>> GeneratePerlinNoise(const std::vector<std::vector<float>>& baseNoise, const std::uint32_t octave);
		static std::vector<std::vector<float>> GeneratePerlinNoise(const std::uint32_t width, const std::uint32_t height, const std::uint32_t octave);
		static Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> CreateTextureFromNoise(ID3D11Device* device, const std::vector<std::vector<float>>& noise, ID3D11Texture2D** texture = nullptr);

	private:
		PerlinNoise();		

		float GetRandomValue();
		std::vector<std::vector<float>> GetEmptyArray(const std::uint32_t width, const std::uint32_t height) const;

		static std::unique_ptr<PerlinNoise> sInstance;
		
		std::random_device mDevice;
		std::default_random_engine mGenerator;
		std::uniform_real_distribution<float> mDistribution;
	};
}
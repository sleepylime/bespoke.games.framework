#include "pch.h"
#include "TextRenderingGame.h"

using namespace std;
using namespace DirectX;
using namespace Library;
using namespace Microsoft::WRL;

namespace TextRendering
{
	const XMVECTORF32 TextRenderingGame::BackgroundColor = Colors::Black;
	const wstring TextRenderingGame::Message = L"Hello, World!";

	TextRenderingGame::TextRenderingGame(function<void*()> getWindowCallback, function<void(SIZE&)> getRenderTargetSizeCallback) :
		Game(getWindowCallback, getRenderTargetSizeCallback)
	{
	}

	void TextRenderingGame::Initialize()
	{
		SpriteManager::Initialize(*this);
		BlendStates::Initialize(mDirect3DDevice.Get());

		mKeyboard = make_shared<KeyboardComponent>(*this);
		mComponents.push_back(mKeyboard);
		mServices.AddService(KeyboardComponent::TypeIdClass(), mKeyboard.get());

		mFont = make_shared<SpriteFont>(mDirect3DDevice.Get(), L"Content\\Fonts\\Arial_36_Regular.spritefont");

		Game::Initialize();
	}

	void TextRenderingGame::Shutdown()
	{
		BlendStates::Shutdown();
		SpriteManager::Shutdown();

		Game::Shutdown();
	}

	void TextRenderingGame::Update(const GameTime &gameTime)
	{
		if (mKeyboard->WasKeyPressedThisFrame(Keys::Escape))
		{
			Exit();
		}

		XMFLOAT2 tempViewportSize(mViewport.Width, mViewport.Height);
		XMVECTOR viewportSize = XMLoadFloat2(&tempViewportSize);

		XMVECTOR messageSize = mFont->MeasureString(Message.c_str());
		XMStoreFloat2(&mMessagePosition, (viewportSize - messageSize) / 2);
		mMessagePosition.y -= XMVectorGetY(messageSize);

		wostringstream subMessageStream;
		subMessageStream << std::round(gameTime.TotalGameTimeSeconds().count());
		mSubMessage = subMessageStream.str();
		messageSize = mFont->MeasureString(mSubMessage.c_str());
		XMStoreFloat2(&mSubMessagePosition, (viewportSize - messageSize) / 2);
		mSubMessagePosition.y += XMVectorGetY(messageSize);

		Game::Update(gameTime);
	}

	void TextRenderingGame::Draw(const GameTime &gameTime)
	{
		mDirect3DDeviceContext->ClearRenderTargetView(mRenderTargetView.Get(), reinterpret_cast<const float*>(&BackgroundColor));
		mDirect3DDeviceContext->ClearDepthStencilView(mDepthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
				
		Game::Draw(gameTime);

		SpriteManager::DrawString(mFont, Message.c_str(), mMessagePosition);
		SpriteManager::DrawString(mFont, mSubMessage.c_str(), mSubMessagePosition);

		HRESULT hr = mSwapChain->Present(1, 0);

		// If the device was removed either by a disconnection or a driver upgrade, we must recreate all device resources.
		if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
		{
			HandleDeviceLost();
		}
		else
		{
			ThrowIfFailed(hr, "IDXGISwapChain::Present() failed.");
		}
	}

	void TextRenderingGame::Exit()
	{
		PostQuitMessage(0);
	}
}